package ru.rt;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static final int size = 1_000_000_000;

    public static void main(String[] args) throws IOException {
        generate();
        int[] read = read(size);
        System.out.println("readed");
        Sort cs = new QuickSort(read);
        cs.sortAndTime();


        cs.sortAndTime();

    }

    private static int[] read(int size) throws IOException {
        int[] arr = new int[size];
        int i = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("sort" + size + ".in")));
        while(reader.ready()) {
            arr[i++] = Integer.valueOf(reader.readLine());
        }
        return arr;
    }

    private static void generate() {
        try (BufferedWriter dos = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("sort"+size+".in")))){
            for (int i = 0; i < size; i++) {
                int x = (int)(Math.random()*65536);
                dos.write(String.valueOf(x) + "\n");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

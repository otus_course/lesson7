package ru.rt;

import java.io.*;
import java.util.Scanner;

public class MergeSort extends Sort{
    public MergeSort(int[] arr) {
        super(arr);
    }

    public void sort() {
        sort(0, arr.length - 1);
    }

    private void sort(int l, int r) {
        if (l >= r) return;
        int x = (l + r) / 2;
        sort(l, x);
        sort(x + 1, r);
        if (x - l > 1024) {
            try {
                outerMerge(l, x, r);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            merge(l, x, r);
        }
    }
    static String filename = "sort.tmp";
    protected void outerMerge(int l, int x, int r) throws IOException {
        File out = new File(filename);
        if(!out.exists()){
            out.createNewFile();
        }
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out.getPath())))){
            int a = l;
            int b = x + 1;
            while (a <= x && b <= r) {
                writer.write(String.valueOf((arr[a] < arr[b]) ? arr[a++] : arr[b++])+"\n");
            }
            while (a <= x) {
                writer.write(String.valueOf(arr[a++]) + "\n");
            }
            while (b <= r) {
                writer.write(String.valueOf(arr[b++]) + "\n");
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            int i=l;
            while(reader.ready()) {
                arr[i++] = Integer.valueOf(reader.readLine());
            }
        }
    }

    protected void merge(int l, int x, int r) {
        int[] nArr = new int[r - l + 1];
        int a = l;
        int b = x + 1;
        int m = 0;
        while (a <= x && b <= r) {
            nArr[m++] = (arr[a] < arr[b]) ? arr[a++] : arr[b++];
        }
        while (a <= x) {
            nArr[m++] = arr[a++];
        }
        while (b <= r) {
            nArr[m++] = arr[b++];
        }
        for (int i = l; i <= r; i++) {
            arr[i] = nArr[i-l];
        }
    }

}

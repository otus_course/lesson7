package ru.rt;


public class CountingSort extends Sort{

    public CountingSort(int[] arr) {
        super(arr);
    }

    public void sort() {
        int[] tmpArr = new int[65536];
        int[] nArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            tmpArr[arr[i]]++;
        }
        for (int i = 1; i < tmpArr.length; i++) {
            tmpArr[i] = tmpArr[i] + tmpArr[i-1];
        }
        for (int i = arr.length-1; i >= 0; i--) {
            nArr[--tmpArr[arr[i]]] = arr[i];
        }
        arr = nArr;
    }

}

package ru.rt;

import java.io.*;

public class MergeCombine extends MergeSort{
    public MergeCombine(int[] arr) {
        super(arr);
    }

    @Override
    public void sort() {
        combineSort(0, arr.length - 1);
    }

    private void combineSort(int l, int r) {
        if (l >= r) return;
        int x = (l + r) / 2;

        if(x - l < 1024){
            QuickSort qs = new QuickSort(arr);
            qs.sort(l,x);
            qs.sort(x + 1, r);
        }else{
            combineSort(l, x);
            combineSort(x + 1, r);
        }

        if (x - l >= 1000000) {
            try {
                outerMerge(l, x, r);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else{
            merge(l, x, r);
        }
    }



}
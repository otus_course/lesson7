package ru.rt;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class Tester {
    private ru.rt.Task task;
    private String path;
    private long time = 0;
    public Tester(ru.rt.Task task, String s) {
        this.task = task;
        this.path = s;
    }

    public void runTests() {
        int num = 0;
        while (true){
            String in = path + "/test."+num + ".in";
            String out = path + "/test."+num + ".out";
            File inFile = new File(in);
            File outFile = new File(out);
            if(!inFile.exists() || !outFile.exists()){
                break;
            }
            this.time = System.currentTimeMillis();
            boolean result = runTest(inFile, outFile);
            System.out.println("Test № " + num + " - " + result + ", time: " + (System.currentTimeMillis() - time) + " ms.");
            num++;
        }
    }

    private boolean runTest(File in, File out) {
        try{

            String[] data = Files.readAllLines(in.toPath(), StandardCharsets.UTF_8).toArray(String[]::new);
            String expect = Files.readString(out.toPath(), StandardCharsets.UTF_8);
            String actual = task.run(data);
            return actual.trim().equals(expect.trim());
        }catch (Exception e){
            System.out.println(e.getLocalizedMessage());
        }
        return false;
    }
}

package ru.rt;

public abstract class Sort{
    protected int[] arr;

    public Sort(int[] arr) {
        this.arr = arr;
    }

    public void sortAndTime(){
        long start = System.currentTimeMillis();
        sort();
        System.out.println(this.getClass().getSimpleName()+" size: " + arr.length + " time: " +(System.currentTimeMillis() - start) + " ms.");
    }
    abstract void sort();
}

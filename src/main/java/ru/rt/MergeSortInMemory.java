package ru.rt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class MergeSortInMemory extends Sort{
    public MergeSortInMemory(int[] arr) {
        super(arr);
    }

    public void sort() {
        sort(0, arr.length - 1);
    }

    private void sort(int l, int r) {
        if (l >= r) return;
        int x = (l + r) / 2;
        sort(l, x);
        sort(x + 1, r);
        merge(l, x, r);
    }


    private void merge(int l, int x, int r) {
        int[] nArr = new int[r - l + 1];
        int a = l;
        int b = x + 1;
        int m = 0;
        while (a <= x && b <= r) {
            nArr[m++] = (arr[a] < arr[b]) ? arr[a++] : arr[b++];
        }
        while (a <= x) {
            nArr[m++] = arr[a++];
        }
        while (b <= r) {
            nArr[m++] = arr[b++];
        }
        for (int i = l; i <= r; i++) {
            arr[i] = nArr[i-l];
        }
    }

}

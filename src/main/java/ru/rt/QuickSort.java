package ru.rt;

public class QuickSort extends Sort{

    public QuickSort(int[] arr) {
        super(arr);
    }

    public void sort() {
        sort(0, arr.length - 1);
    }

    public void sort(int l, int r) {
        if (l >= r) return;
        int x = split(l, r);
        sort(l, x - 1);
        sort(x + 1, r);

    }

    private int split(int l, int r) {
        int p = arr[r];
        int a = l - 1;
        for (int m = l; m <= r; m++) {
            if (arr[m] <= p) {
                swap(++a, m);
            }
        }
        return a;
    }

    private void swap(int a, int b) {
        int x = arr[a];
        arr[a] = arr[b];
        arr[b] = x;
    }
}
